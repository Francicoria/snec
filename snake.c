#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "platform-dependent.h"

#define GRID_SIZE 10

typedef struct { int x, y; } Vec2;

typedef enum {
	EMPTY = 0,
	APPLE,
	SNAKE_HEAD,
	SNAKE_TAIL,
} Cell;

typedef Cell Grid[GRID_SIZE][GRID_SIZE];

void print_grid_(Grid g) {
	char buf[1024];
	size_t cursor = 0;
#define print(ch) do { buf[cursor++] = ch; } while (0)
#define prints(str) do { size_t i = 0; while (str[i] != 0) { buf[cursor++] = str[i++]; } } while (0)
	for (size_t y = 0; y < GRID_SIZE; ++y) {
		for (size_t x = 0; x < GRID_SIZE; ++x) {
			Cell c = g[y][x];
			switch (c) {
				case EMPTY: {
					print('.');
				} break;
				case APPLE: {
#ifdef TERMINAL_COLORS
					prints("\x1b[31m");
#endif
					print('o');
				} break;
				case SNAKE_HEAD: {
#ifdef TERMINAL_COLORS
					prints("\x1b[32m");
#endif
					print('@');
				} break;
				case SNAKE_TAIL: {
#ifdef TERMINAL_COLORS
					prints("\x1b[32m");
#endif
					print('a');
				} break;
				default: {
					assert(false && "unreachable");
				}
			}
#ifdef TERMINAL_COLORS
			prints("\x1b[m");
#endif
			print(' ');
		}
		print('\n');
	}

	printf("%.*s", (int)cursor, buf);
}

#define print_grid(g) do { \
	print_grid_((g)); \
	printf("\e[%uA", GRID_SIZE); \
	wait_ms(100); \
} while (0)

static inline bool Vec2equal(Vec2 v1, Vec2 v2) {
	return (v1.x == v2.x) && (v1.y == v2.y);
}

int main(void) {
	enable_terminal_codes();
	set_blocking_mode_and_cursor_visibility(false, false);

	static_assert(EMPTY == 0, "EMPTY must be 0 for the initialization below.");
	Grid grid = {0};

	Vec2 direction = {0};

	Vec2 head_position = {.x = 3, .y = 4};
	Vec2 tail_positions[GRID_SIZE * GRID_SIZE] = {0};
	size_t tail_size = 0;

	Vec2 apple_position = {.x = 6, .y = 4};

	enum { WIN, LOST, PLAYING } status = PLAYING;
	while (status == PLAYING) {
		Vec2 prev_direction = direction;

		char cmd = get_command();
		switch (cmd) {
			case 'q': goto quit_program;
			case 'w': direction.x =  0; direction.y = -1; break;
			case 's': direction.x =  0; direction.y =  1; break;
			case 'a': direction.x = -1; direction.y =  0; break;
			case 'd': direction.x =  1; direction.y =  0; break;
			default:;
		}
		if (direction.x != 0 && -direction.x == prev_direction.x) direction.x = prev_direction.x;
		if (direction.y != 0 && -direction.y == prev_direction.y) direction.y = prev_direction.y;

		// Before making the snake move, remove the last tail so that it doesn't leave a trail.
		if (tail_size > 0) grid[tail_positions[tail_size-1].y][tail_positions[tail_size-1].x] = EMPTY;
		else               grid[head_position.y][head_position.x] = EMPTY;

		for (int i = tail_size - 1; i > 0; --i) {
			tail_positions[i] = tail_positions[i - 1];
		}
		tail_positions[0] = head_position;

		head_position.y += direction.y;
		head_position.x += direction.x;

		for (size_t i = 0; i < tail_size; ++i) {
			if (Vec2equal(head_position, tail_positions[i])) {
				status = LOST;
				break;
			}
		}
		// Check if the apple was eaten, and if it was move it somewhere where the snake is not.
		if (Vec2equal(head_position, apple_position)) {
			tail_size += 1;
			tail_positions[tail_size-1] = head_position;

			while (Vec2equal(head_position, apple_position)) {
				apple_position.y = rand()%GRID_SIZE;
				apple_position.x = rand()%GRID_SIZE;
			}
		}

		grid[apple_position.y][apple_position.x] = APPLE;

		if (head_position.y < 0) head_position.y = GRID_SIZE-1;
		if (head_position.y > GRID_SIZE-1) head_position.y = 0;
		if (head_position.x < 0) head_position.x = GRID_SIZE-1;
		if (head_position.x > GRID_SIZE-1) head_position.x = 0;


		for (size_t i = 0; i < tail_size; ++i) {
			grid[tail_positions[i].y][tail_positions[i].x] = SNAKE_TAIL;
		}
		grid[head_position.y][head_position.x] = SNAKE_HEAD;

		print_grid(grid);
	}
	switch (status) {
		case LOST: {
			printf("GAME OVER!\n");
		} break;
		case WIN: {
			printf("YOU WON!\n");
		} break;
		case PLAYING: {
			assert(false && "unreachable");
		} break;

	}

quit_program:
	print_grid_(grid);

	set_blocking_mode_and_cursor_visibility(true, true);
	return 0;
}
