#!/bin/sh

set -xe

CC=gcc
CFLAGS="-Wall -Wextra -Wpedantic -O2"
SRC="platform-windows.c snake.c"

${CC} ${CFLAGS} -o snake ${SRC}
${CC} ${CFLAGS} -DTERMINAL_COLORS -o snake-colors ${SRC}
