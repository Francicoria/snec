#include <stdbool.h>
#include <stdio.h>

#define WIN32_MEAN_AND_LEAN
#include <windows.h>

#ifndef ENABLE_VIRTUAL_TERMINAL_PROCESSING
#define ENABLE_VIRTUAL_TERMINAL_PROCESSING 0x0004
#endif

#include "platform-dependent.h"

void wait_ms(size_t ms) { Sleep(ms); }

void set_blocking_mode_and_cursor_visibility(bool blocking, bool cursor_visible) {
	HANDLE win_stdout = GetStdHandle(STD_OUTPUT_HANDLE);
	// Windows is dumb, so instead of enabling blocking/non-blocking mode once you have to ask everytime you want input if the user entered something or didn't, and get the input if they did.  ._.
	(void)blocking;

	CONSOLE_CURSOR_INFO cursorInfo;
	GetConsoleCursorInfo(win_stdout, &cursorInfo);
	cursorInfo.bVisible = cursor_visible;
	SetConsoleCursorInfo(win_stdout, &cursorInfo);
}

void enable_terminal_codes(void) {
	HANDLE winConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	DWORD winConsoleMode = 0;
	GetConsoleMode(winConsole, &winConsoleMode);
	SetConsoleMode(winConsole, winConsoleMode | ENABLE_VIRTUAL_TERMINAL_PROCESSING);
}

char get_command(void) {
	HANDLE win_stdin = GetStdHandle(STD_INPUT_HANDLE);

	DWORD input_events;
	if (!GetNumberOfConsoleInputEvents(win_stdin, &input_events)) {
		fprintf(stderr, "ERROR: Couldn't get the number of input events: 0x%lx\n", GetLastError());
		return 1;
	}

	if (input_events != 0) {
#define INPUT_BUFFER_SIZE 64
		INPUT_RECORD input[INPUT_BUFFER_SIZE];
		DWORD input_events_read;
		ReadConsoleInputA(win_stdin, input, INPUT_BUFFER_SIZE, &input_events_read);
		for (size_t i = 0; i < input_events_read; ++i) {
			if (input[i].EventType != KEY_EVENT) continue;
			KEY_EVENT_RECORD e = input[i].Event.KeyEvent;
			if (e.bKeyDown) continue;
			return e.uChar.AsciiChar;
		}
	}
	return 0;
}
