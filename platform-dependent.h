#ifndef PLAT_DEPENDENT_H
#define PLAT_DEPENDENT_H

void set_blocking_mode_and_cursor_visibility(bool blocking, bool cursor_visible);
void enable_terminal_codes(void);
char get_command(void);
void wait_ms(size_t ms);

#endif // PLAT_DEPENDENT_H
